# JDR Dispoot

## Géographie

Île dont la taille se situe entre la Corse et l'Angleterre  
4 régions :

* Bawira (inrégion de départ)
  * La capitale est Syned(?)
* Rudesh (Ouest)
* Tikir (Nord)
* Issiel (encore plus au Nord)

## Historique du groupe

On se connait tous du service militaire.
Nous avons tous environ 25 ans.

1. Luc KeenEye (Adrien):

    * Ranger, guide
    * Connait déjà Liad, la ville centrale de l'aventure
    * Survie dans la nature
    * 8 points de Cosmos

2. Vesemir (Hugo) :

    * Chasseur, chasseur de primes, défense d'agriculteurs
    * Viande, cuir
    * 8 points de Cosmos

3. Théobald (Cyril) :

    * Escrime, escorte de caravane
    * 8 points de Cosmos

## Équipement

1. Luc KeenEye :

    * Arc court
    * Torche

2. Vesemir :

    * Javelots
    * Torche

3. Théobald :

    * Claymore
    * Armure cloutée
    * Torche

## Histoire

### Introduction

* Vesemir a trouvé le contrat par Dalibor, un réprésentant de la mine et a récupéré l'aide de Théobald et Luc.
* Un gros monstre effraie les travailleurs de la mine de fer.
* Un cadavre a été retrouvé sur le bord de la route
  * Pas d'info supplémentaire disponible car le cadavre a été enterré rapidement pour étouffer l'affaire
  * Une interrogation des mineurs les agiterait et énervera Dalibor.

### La taverne

* On boit la cuvée du printemps
* Sale ambiance
* Dalibor nous rejoint
  * Théobald voit qu'il est stressé
  * Il veut une grosse tête de bête pour rassurer la population
  * Il dit qu'il est facile de trouver des gros monstres et du gibier dans la forêt
    * Il s'avère que c'est faux (Luc double 6)
  * Limite du contrat : 48h
  * 5 pièces d'or + 1 si très grosse tête

### La route

* Rien en haut de la pente
* Luc aperçoit un appendice (queue) qui disparait rapidement (Luc double 6)

### Retour à la taverne

* Discussion du groupe
  * On confirme que la rumeur a un fond de vérité
  * On décide de tuer le monstre même si on dépasse la limite de temps

### Crocodi Dundee

* Luc achète de la corde et une longue vue
* Crocodi le met en garde

### Retour sur la route

* Vesemir escalade (difficilement) sans ses javelots
* Luc vient l'aider
